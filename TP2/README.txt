OpenMrs deployment on Docker : 
1) run Docker on your computer
2) open a terminal in this folder
3) in the terminal, run the command : Docker-compose up

Jmeter with dockerized OpenMrs:
1) open Jmeter
2) upload the file : Four-Workloads.jmx
3) change the IP adress of the workload if needed
4) right click on the workload and press start
5) see the results in "view results"