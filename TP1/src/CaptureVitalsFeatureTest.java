import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.Test;

public class CaptureVitalsFeatureTest {

	@Test
	public void testCaptureVitalsPatientExists1() {
		for(int i = 0; i< 150000; i++) {
			i+=i*i;
			int j = i*i*i*i;
			int k = i * j;
		}
		CaptureVitalsModule test = new CaptureVitalsModule();
		test.FindPatientVitals1();
		test.FindPatientVitals11();
        assertEquals(0, 0, "Patient exists 1");
	}
	@Test
	public void testCaptureVitalsPatientExists2() {
		for(int i = 0; i< 150000; i++) {
			i+=i*i;
			int j = i*i*i*i;
			int k = i * j;
		}
		CaptureVitalsModule test = new CaptureVitalsModule();
		test.FindPatientVitals2();
		test.FindPatientVitals12();
        assertEquals(0, 0, "Patient exists 2");
	}
	@Test
	public void testCaptureVitalsPatientExists3() {
		for(int i = 0; i< 150000; i++) {
			i+=i*i;
			int j = i*i*i*i;
			int k = i * j;
		}
		CaptureVitalsModule test = new CaptureVitalsModule();
		test.FindPatientVitals3();
		test.FindPatientVitals13();
        assertEquals(0, 0, "Patient exists 3");
	}
	@Test
	public void testCaptureVitalsPatientExists4() {
		for(int i = 0; i< 150000; i++) {
			i+=i*i;
			int j = i*i*i*i;
			int k = i * j;
		}
		CaptureVitalsModule test = new CaptureVitalsModule();
		test.FindPatientVitals4();
		test.FindPatientVitals14();
        assertEquals(0, 0, "Patient exists 1");
	}
	@Test
	public void testCaptureVitalsPatientExists5() {
		for(int i = 0; i< 150000; i++) {
			i+=i*i;
			int j = i*i*i*i;
			int k = i * j;
		}
		CaptureVitalsModule test = new CaptureVitalsModule();
		test.FindPatientVitals5();
		test.FindPatientVitals15();
        assertEquals(0, 0, "Patient exists 2");
	}

	@Test
	public void testCaptureVitalsPatientDoesNotExist1() {
		for(int i = 0; i< 150000; i++) {
			i+=i*i;
			int j = i*i*i*i;
			int k = i * j;
		}
		CaptureVitalsModule test = new CaptureVitalsModule();
		test.FindPatientVitals6();
		test.FindPatientVitals16();
        assertEquals(0, 0, "Patient doesn't exist 1");
	}
	@Test
	public void testCaptureVitalsPatientDoesNotExist2() {
		for(int i = 0; i< 150000; i++) {
			i+=i*i;
			int j = i*i*i*i;
			int k = i * j;
		}
		CaptureVitalsModule test = new CaptureVitalsModule();
		test.FindPatientVitals7();
		test.FindPatientVitals17();
        assertEquals(0, 0, "Patient doesn't exist 1");
	}
	@Test
	public void testCaptureVitalsPatientDoesNotExist3() {
		for(int i = 0; i< 150000; i++) {
			i+=i*i;
			int j = i*i*i*i;
			int k = i * j;
		}
		CaptureVitalsModule test = new CaptureVitalsModule();
		test.FindPatientVitals8();
		test.FindPatientVitals18();
        assertEquals(0, 0, "Patient doesn't exist 1");
	}
	@Test
	public void testCaptureVitalsPatientDoesNotExist4() {
		for(int i = 0; i< 150000; i++) {
			i+=i*i;
			int j = i*i*i*i;
			int k = i * j;
		}
		CaptureVitalsModule test = new CaptureVitalsModule();
		test.FindPatientVitals9();
		test.FindPatientVitals19();
        assertEquals(0, 0, "Patient doesn't exist 1");
	}
	@Test
	public void testCaptureVitalsPatientDoesNotExist5() {
		for(int i = 0; i< 150000; i++) {
			i+=i*i;
			int j = i*i*i*i;
			int k = i * j;
		}
		CaptureVitalsModule test = new CaptureVitalsModule();
		test.FindPatientVitals10();
		test.FindPatientVitals20();
        assertEquals(0, 0, "Patient doesn't exist 1");
	}
	
	@Test
	public void testCaptureVitalsRetrieveAllPatientsWithVitals() {
		for(int i = 0; i< 150000; i++) {
			i+=i*i;
			int j = i*i*i*i;
			int k = i * j;
		}
        assertEquals(0, 0, "Retrieve all patients with vitals");
	}
	@Test
	public void testCaptureVitalsRetrieveAllPatientsWithoutVitals() {   
		for(int i = 0; i< 150000; i++) {
			i+=i*i;
			int j = i*i*i*i;
			int k = i * j;
		}
        assertEquals(0, 0, "Retrieve all patients without vitals");
	}


}
