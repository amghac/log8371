import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.Test;

public class FindPatientRecordFeatureTest {

	@Test
	public void testFindPatientRecordPatientExists1() {
		for(int i = 0; i< 150000; i++) {
			i+=i*i;
			int j = i*i*i*i;
			int k = i * j;
		}
		FindPatientRecordModule test = new FindPatientRecordModule();
		test.FindPatientRecord1();
        assertEquals(0, 0, "Patient John Doe exists");
	}
	@Test
	public void testFindPatientRecordPatientExists2() {
		for(int i = 0; i< 150000; i++) {
			i+=i*i;
			int j = i*i*i*i;
			int k = i * j;
		}
		FindPatientRecordModule test = new FindPatientRecordModule();
		test.FindPatientRecord2();
        assertEquals(0, 0, "Patient James Micheals exists");
	}
	@Test
	public void testFindPatientRecordPatientExists3() {
		for(int i = 0; i< 150000; i++) {
			i+=i*i;
			int j = i*i*i*i;
			int k = i * j;
		}
		FindPatientRecordModule test = new FindPatientRecordModule();
		test.FindPatientRecord3();
        assertEquals(0, 0, "Patient Michael Jackson exists");
	}
	@Test
	public void testFindPatientRecordPatientExists4() {
		for(int i = 0; i< 150000; i++) {
			i+=i*i;
			int j = i*i*i*i;
			int k = i * j;
		}
		FindPatientRecordModule test = new FindPatientRecordModule();
		test.FindPatientRecord4();
        assertEquals(0, 0, "Patient Jack Jones exists");
	}
	@Test
	public void testFindPatientRecordPatientExists5() {
		for(int i = 0; i< 150000; i++) {
			i+=i*i;
			int j = i*i*i*i;
			int k = i * j;
		}
		FindPatientRecordModule test = new FindPatientRecordModule();
		test.FindPatientRecord5();
        assertEquals(0, 0, "Patient Michel Tremblay exists");
	}
	
	@Test
	public void testFindPatientRecordPatientDoesNotExist1() {
		for(int i = 0; i< 150000; i++) {
			i+=i*i;
			int j = i*i*i*i;
			int k = i * j;
		}
        assertEquals(0, 0, "Patient serge michel doesn't exist");
	}
	@Test
	public void testFindPatientRecordPatientDoesNotExist2() {
		for(int i = 0; i< 150000; i++) {
			i+=i*i;
			int j = i*i*i*i;
			int k = i * j;
		}
        assertEquals(0, 0, "Patient zak girard doesn't exist");
	}
	@Test
	public void testFindPatientRecordPatientDoesNotExist3() {
		for(int i = 0; i< 150000; i++) {
			i+=i*i;
			int j = i*i*i*i;
			int k = i * j;
		}
        assertEquals(0, 0, "Patient kevin jack doesn't exist");
	}
	@Test
	public void testFindPatientRecordPatientDoesNotExist4() {
		for(int i = 0; i< 150000; i++) {
			i+=i*i;
			int j = i*i*i*i;
			int k = i * j;
		}
        assertEquals(0, 0, "Patient ginette lemay doesn't exist");
	}
	@Test
	public void testFindPatientRecordPatientDoesNotExist5() {
		for(int i = 0; i< 150000; i++) {
			i+=i*i;
			int j = i*i*i*i;
			int k = i * j;
			
		}
        assertEquals(0, 0, "Patient jeanne blo doesn't exist");
	}
	@Test
	public void testFindPatient() {  
		FindPatientRecordModule test = new FindPatientRecordModule();
        assertEquals(true, test.FindPatientRecord("Michel Tremblay"), "Retrieve existing patient record");
	}		@Test
	public void testFindPatient() {  
		FindPatientRecordModule test = new FindPatientRecordModule();
        assertEquals(true, test.FindPatientRecord("Michel Tremblay"), "Retrieve existing patient record");
	}		@Test
	public void testFindPatient() {  
		FindPatientRecordModule test = new FindPatientRecordModule();
        assertEquals(true, test.FindPatientRecord("Michel Tremblay"), "Retrieve existing patient record");
	}










	@Test
	public void testFindPatient() {  
		FindPatientRecordModule test = new FindPatientRecordModule();
        assertEquals(true, test.FindPatientRecord("Michel Tremblay"), "Retrieve existing patient record");
	}



}
